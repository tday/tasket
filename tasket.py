#! /usr/bin/env python3 
import argparse
#import cmd2
import os
import sys

import time
import calendar
import pickle

from tasket2 import TaskList, Task

#Premable - grab the args

local_time = time.localtime()
calendar.setfirstweekday(6)


parser = argparse.ArgumentParser(description="Maintain a list of needful things to do.")
group = parser.add_mutually_exclusive_group()

parser.add_argument("-t","--task", type=str, help="Add a new task to the list - use single or double quotes")
group.add_argument("-d", "--done", type=int, help="Complete a task")
group.add_argument("-x", "--skip", type= int, help="Skip a task")
group.add_argument("-s", "--show", type= int, help="Show a (specific) task")

args = parser.parse_args()
task = args.task
done = args.done
skip = args.skip
show = args.show

#Build and maintain the list
tasklist = TaskList()
tasklist.load_from_pickle('data.pickle')

if task:
    tasklist.add_task(task)
    tasklist.save_to_pickle('data.pickle')



#Show the current tasks

print('\n')
print(time.strftime("%a %b %d, %H:%M:%S %z %Y"))
calendar.prmonth(local_time.tm_year,local_time.tm_mon)
print('\n')
print(tasklist.get_tasks())
