import pickle
from enum import Enum

class Status(Enum):
    Open = 'open'
    Closed = 'closed'

class TaskList:
    def __init__(self, tasklist = []):
        self.tasklist = tasklist
    
    def get_tasks(self):
        # return self.tasklist
        return "\n".join([task.get_task() for task in self.tasklist])
    
    def add_task(self, task):
        new_task = Task(task, len(self.tasklist))
        self.tasklist.append(new_task)
    
    def save_to_pickle(self, filename):
        with open(filename, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(self.tasklist, f, pickle.HIGHEST_PROTOCOL)
    
    def load_from_pickle(self, filename):
        try:
            with open(filename, 'rb')as f:
                self.tasklist = pickle.load(f)

        except:
            with open(filename, 'wb') as f:
                # Pickle the 'data' dictionary using the highest protocol available.
                pickle.dump(self.tasklist, f, pickle.HIGHEST_PROTOCOL)
        
        return self.tasklist
            

class Task:
    def __init__(self, task, index):
        self.task = task
        self.status = Status.Open
        self.index = index
    
    def get_task(self):
        return f'{self.index}: {self.status.value}: {self.task}'
    
    def set_status(self, status:Status):
        self.status = status
    
